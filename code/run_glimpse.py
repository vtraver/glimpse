import glob
import pickle
import numpy as np
from scipy.spatial.distance import pdist
from timeit import default_timer as timer
import matplotlib.pylab as plt

import pandas as pd

SAVAM_PATH = '/ssd/datasets/SAVAM/'

t_pos = 0  # 1st column
x_pos_left = 1  # 2nd column
y_pos_left = 2  # 3rd column

# the ones we'll use
x_pos = x_pos_left
y_pos = y_pos_left

W_SAVAM = 1920  # x-dim
H_SAVAM = 1080  # y-dim

time_units_per_sec = 1000000.0  # approx
time_units_per_frame = 40000.0  # approx
secs_per_minute = 60.0

NOT_AVAIL = 0

# Ripley's K function
# output ratio corresponds to Eq (1) in paper
def ripleyK(points, scale, area):
    D = pdist(points)
    num_nearest = np.sum(D < scale)
    n = points.shape[0]
    num_pairs = n * (n - 1) / 2
    lambda_density = n / area
    ratio = num_nearest / num_pairs
    return num_nearest / (lambda_density * n), ratio  # K Ripley's function, ratio of nearest to total points


def check_data_valid(xy):
    mean = xy.mean()
    return not np.isnan(mean)


class TemporalScore:
    def __init__(self):
        self.xy = None
        self.n_users = None
        self.HW = None

    # loads file for a single user name
    # and returns NumPy array with 3 columns: time stamp, x and y (left view)
    def load_user_gaze(self, filename):
        xy = np.loadtxt(filename)
        return xy[:, [t_pos, x_pos, y_pos]]

    # returns NumPy array of Nx2 (N=#frames, 2 for x,y)
    # process:
    # - interpolation to remove missing values
    # - within-frame average of x and y values to keep single value per frame
    # - x,y normalization to [0,1]
    def preprocess_user_gaze(self, txy):
        # 1. interpolate when data is not available
        txy[txy[:, 1] == NOT_AVAIL, 1] = np.nan
        txy[txy[:, 2] == NOT_AVAIL, 2] = np.nan
        df = pd.DataFrame(txy)
        interp_method = 'quadratic'
        interp_method = 'linear'
        df = df.interpolate(method=interp_method,
                            limit_direction='both')  # without 'both', NaNs remain at the beginning
        txy = np.array(df)

        # 2. from time units to frame units to have single x,y per time
        first_time = txy[0, 0]
        last_time = txy[-1, 0]
        total_time = last_time - first_time
        nframes = int(total_time / time_units_per_frame)
        nsecs = total_time / time_units_per_sec
        nmins = nsecs / secs_per_minute
        xy = np.zeros((nframes, 2))
        txy[:, 0] -= first_time  # "reset time": consider time is 0 first
        # keep averages in x and y for all data within each frame time interval
        for k in range(nframes):
            times_per_frame = np.logical_and(k * time_units_per_frame <= txy[:, 0],
                                             txy[:, 0] < (k + 1) * time_units_per_frame)
            which = np.where(times_per_frame)[0]
            xy[k, 0] = np.mean(txy[which, 1])
            xy[k, 1] = np.mean(txy[which, 2])

        # 3. normalize x,y data to [0,1]
        bNormalize = True
        if bNormalize:
            xy[:, 0] /= W_SAVAM
            xy[:, 1] /= H_SAVAM

        bDisplay = False
        if bDisplay:
            plt.plot(xy)
            plt.show(block=True)

        return xy

    # returns a 3D array, with these axis:
    # 0: user index
    # 1: implicit time (frame number)
    # 2: x,y data
    # Example: xy[2][10][0] = x value at frame t of user 2
    def load_data(self, data_path, video_id):
        # TODO: consider only FWD or BWD data?
        files = glob.glob(data_path + '/' + 'v' + str(video_id).zfill(2) + '_*.txt')
        self.n_users = len(files)
        bVerbose = False
        if bVerbose:
            print(files)
        data = []
        for filename in files:
            # print("processing file",filename)
            txy = self.load_user_gaze(filename)
            xy = self.preprocess_user_gaze(txy)
            if check_data_valid(xy):
                # plot_xy_singleUser_singleVideo(xy,os.path.basename(filename))
                data.append(xy)
            else:
                print("data for file", filename, "not included")
        return np.array(data)

    # Input: 3D array NxTx2 for N users, T frames and 2 coordinates (x,y)
    #        (example: xy[2][10][0] = x value at frame t=10 of user 2)
    def compute_homegeneity_index(self, data, groupBy, user=None, bDisplay=False, tw=5, ss=0.1):
        # groupBy:
        #    'perUser', then user should be provided and, optionally, temporal window
        #    'perFrame', then all userss are considered for each frame

        T = data.shape[1]
        U = data.shape[0]
        homogeneity = np.zeros(T)
        scale = ss  # distance threshold
        area = 1  # width x height = 1 x 1 (if coordinates are normalised)
        temporalWindow = tw
        # user = np.random.randint(0,U,1)[0]  # TODO: provide as argument
        measure = 'KRippley'
        measure = 'NNratio'
        measure_idx = {'KRippley': 0, 'NNratio': 1}
        for t in range(T):  # temporalWindow,T):
            if groupBy is 'perFrame':
                data_ = np.reshape(data[:, np.maximum(0, t - temporalWindow): np.minimum(T, t + temporalWindow), :],
                                   (-1, 2))
                homogeneity[t] = ripleyK(data_, scale=scale, area=area)[measure_idx[measure]]
            else:
                homogeneity[t] = \
                ripleyK(data[user, np.maximum(0, t - temporalWindow): np.minimum(T, t + temporalWindow), :],
                        scale=scale, area=1)[measure_idx[measure]]
        if bDisplay:
            plt.plot(homogeneity)
            plt.title("groupBy:" + groupBy + ", scale: " + str(scale) + ", area: " + str(area) + ", user: " + str(
                user) + ", time window: " + str(temporalWindow))
            plt.show(block=True)
        return homogeneity

    def compute_homegeneity_index_limitedObservers(self, data, n_observers=np.infty, n_combi_max=np.infty):
        n_observers_total = data.shape[0]
        n_observers = int(np.minimum(n_observers, n_observers_total))

        nc = int(comb(n_observers_total, n_observers))
        combs_iterable = combinations(range(n_observers_total), n_observers)
        n_combi_max = int(np.minimum(nc, n_combi_max))
        if n_combi_max < 4:  # decide one approach or another for computational reasons
            combs_sample = x_gen_sample(combs_iterable, n_combi_max, nc, n_observers)
        else:
            combs_sample = np.array(
                [np.random.choice(n_observers_total, n_observers, replace=False) for k in range(n_combi_max)])
        nc_sel = combs_sample.shape[0]

        bVerbose = False
        if bVerbose:
            print("n_observers_total", n_observers_total)
            print("n_observers", n_observers)
            print("combinations", nc)

        h = []
        for combi in combs_sample:
            # print(data[combi,:].shape)
            h.append(self.compute_homegeneity_index(data[combi, :], groupBy='perFrame', bDisplay=False))

        h = np.array(h)  # convert list to np.array for easier computations
        print("h_array.shape", h.shape)

        h_mean, h_std = np.mean(h, axis=0), np.std(h, axis=0)
        bDisplay = True
        if bDisplay:
            plt.clf()
            T = h.shape[1]
            # print("h_mean.shape, h_std.shape",h_mean.shape,h_std.shape)
            plt.plot(h_mean, label='mean homogeneity index')
            plt.xlabel('frames')
            plt.ylabel('h')
            plt.fill_between(range(T), h_mean - h_std, h_mean + h_std, color='b', alpha=0.2, label='std err h')
            plt.title(str(nc_sel) + " combinations of " + str(n_observers) + " observers")
            plt.show(block=False)
            filename = "comb_" + str(nc_sel).zfill(3) + "_users_" + str(n_observers).zfill(3) + ".png"
            plt.savefig(filename, bbox_inches='tight')
        return h_mean, h_std, h


def run_glimpse_on_SAVAM(videos_id):
    ts = TemporalScore()
    tw_gaze, ss_gaze = 5, 0.1 # SAVAM hyperparams ($\theta_t$ and $\theta_s$ in the paper)
    bRawGazeData = True
    subFolder = 'raw_gaze_data' if bRawGazeData else 'filtered_gaze_data'
    gaze_data_path = SAVAM_PATH + 'gaze_data/' + subFolder
    times_glimpse = []
    scores_videos = {}
    for video_id in videos_id:
        data = ts.load_data(gaze_data_path, video_id)
        h_gaze = ts.compute_homegeneity_index(data, groupBy='perFrame', bDisplay=False, tw=tw_gaze, ss=ss_gaze)
        scores_videos[video_id] = h_gaze  # 1D temporal salience score
    return scores_videos


def plot_scores_video(video_id, score):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ratio=200
    ax.set_aspect(1.0/ax.get_data_ratio()*ratio)

    plt.title('GLIMPSE on video v' + str(video_id))
    plt.xlabel('time (frame)')
    plt.ylabel('salience score')
    plt.grid(True,ls='dotted')

    plt.plot(score)
    plt.show(block=True)

def plot_scores_videos(scores):
    for video_id in scores.keys():
        plot_scores_video(video_id, scores[video_id])


if __name__ == "__main__":

    selected_video_ids = [30, 36, 43]  # videos in Fig. 5

    # compute the scores on selected SAVAM videos
    scores = run_glimpse_on_SAVAM(selected_video_ids)

    # plot computed scores
    plot_scores_videos(scores)

