mport pickle
import os
import re
import glob
import numpy as np
from PIL import Image

def get_allFiles_ids_names_SAVAM(path):
    from pathlib import Path
    import re
    leftRight='left'
    files = [f.as_posix() for f in Path(path).rglob('v??'.zfill(2) + "*"+leftRight+"*.avi")]
    names = [re.search(r'\w*_left', file).group()[4:-5] for file in files]
    ids=[re.search(r'v[0-9][0-9]',file).group()[1:] for file in files]
    videos_id2names = {int(id):name for id,name in zip(ids,names)}
    names2id = {name: id for id, name in videos_id2names.items()}
    id2files={int(ids[i]):files[i] for i in range(len(files))}
    return files,videos_id2names, names2id,id2files

def get_score_filename(method,algo,video_id,params):
    path = './scores-' + method + "-" + algo
    if method=='gaze':
        theta_s, theta_t = params['theta_s'], params['theta_t']
        infix = 'thetaS_' + str(theta_s) + "_thetaT_" + str(theta_t) + "_"
    else:
        infix = "3_" if method=='multiduration' and algo=='minfo' else ""
    nzeros = 2
    filename = path + "/" + "temporal_score_" + method + "_" + infix + str(video_id).zfill(nzeros) + ".p"
    bRetry = True#(method=='multiduration')
    while bRetry:
        try:
            filename = path + "/" + "temporal_score_" + method + "_" + infix + str(video_id).zfill(nzeros) + ".p"
            infile = open(filename, "rb")
            bRetry=False
        except:
            nzeros+=1
    print("method:",method, "algo:",algo)
    print("filename:",filename)
    return filename, path

def load_score_file(method,algo,video_id,params):
    #method = 'gbvs'
    #algo = 'mutualinfo'
    filename,path=get_score_filename(method,algo,video_id,params)
    infile = open(filename, "rb")
    h = pickle.load(infile)
    #print(h.shape)
    infile.close()
    return h

def save_score_file(h,method,algo,video_id,params):
    filename,path=get_score_filename(method,algo,video_id,params)
    if not os.path.exists(path):
        os.makedirs(path)
    outfile = open(filename, "wb")
    pickle.dump(h,outfile)
    # print(h.shape)
    outfile.close()

def getVideoLength(folder, ext):
    # print("folder",folder)
    list_of_files = glob.glob(folder + "/*." + ext)
    # print(list_of_files)
    max = -np.inf
    for file in list_of_files:
        num = int(re.search('frame_(\d*)', file).group(1))  # assuming filename is "filexxx.txt"
        # print(num)# compare num to previous max, e.g.
        max = num if num > max else max  # set max = 0 before for-loop
    return max


def loadSalienceMap(path,video,frames):
    return np.array([np.array(Image.open(path+video+"/frame_"+str(i).zfill(4)+".pgm"))/255.0 for i in frames])

def loadHoeffD(method,algo):
    '''
    From: Luis Leiva (19/11/20)
    Cada CSV tiene 41 líneas: cada línea es la correlación del método frente a gaze-points para un vídeo dado.
    Creo que el número de línea no coincide con el número de vídeo; pero bueno, es irrelevante para los boxplots.

    Respecto a los baselines, no se puede calcular la correlación porque son series de valores constantes. Hoeffding da N/A.
    '''

    path='./hoeffd'
    filename = path + '/scores-' + method + "-" + algo + ".csv" # note that 'scores' could be confusing; the files contain Hoeffding's D measure
    return np.genfromtxt(filename)


if __name__ == "__main__":
    source_data='tased'
    source_data='multiduration'
    scoring_algorithm='points'
    scoring_algorithm='spread'
    data = loadHoeffD(source_data,scoring_algorithm)
    print(data.shape)
    data=np.array(data)
    print(data.shape)