from io_utils import get_allFiles_ids_names_SAVAM
import pickle
from scipy.ndimage.morphology import binary_dilation
import numpy as np
from scipy import stats
import matplotlib.pylab as plt

PATH_SAVAM='/ssd/datasets/SAVAM/' 
PATH_SCORES='../data/temporal-salience-scores/'
PATH_PLOTS='../out/'

config = {
    # SAVAM ids and names
    'videos_path_SAVAM': PATH_SAVAM+'video_sources/',

    'glimpse_scores_path': PATH_SCORES+'/scores-gaze-points/',
    
    # GLIMPSE hyperparameters
    'theta_s': 0.1,
    'theta_t': 5,

    # output path
    'plot_path': PATH_PLOTS
}

plot_styles = {
    'names': {'glimpse': 'GLIMPSE'},
    'line': {'glimpse': 'r-'},
    'alpha': {'glimpse': 0.9},
    'width': {'glimpse': 2},
}


def load_SAVAM_filenames():
    global files, id2names, names2id, id2files

    videos_path = config['videos_path_SAVAM']
    files, id2names, names2id, id2files = get_allFiles_ids_names_SAVAM(videos_path)
    ids = list(id2files.keys())
    return ids

def load_scores(video_id):
    global files, id2names, names2id, id2files

    all_rhos, all_taus, all_ious = [], [], []
    id = video_id

    # load glimpse score
    file_name_glimpse = config['glimpse_scores_path'] + "temporal_score_gaze" \
                        + "_thetaS_" + str(config['theta_s']) \
                        + "_thetaT_" + str(config['theta_t']) \
                        + "_" + str(id).zfill(2) + ".p"

    with open(file_name_glimpse, 'br') as handle:
        glimpse_scores = np.array(pickle.load(handle))

    return glimpse_scores


def plot_scores(id, glimpse, bDisplay=False, bLegend=True):
    SMALL_SIZE = 11
    MED_SIZE = 15
    LARGE_SIZE = 30
    plt.clf()
    plt.rc('font', family='serif')
    plt.rcParams['axes.labelsize'] = MED_SIZE
    plt.rc('legend', fontsize=SMALL_SIZE)
    plt.grid(linestyle=':', alpha=0.5)
    plt.figure(111)
    ax = plt.subplot()
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.set_aspect(150)

    # plt.rc('xtick', labelsize='x-small')
    # plt.rc('ytick', labelsize='x-small')

    plt.xlabel('frame number')
    plt.ylabel('score')
    plt.title('video v' + str(id))

    for method in ['glimpse']:
        plt.plot(eval(method), plot_styles['line'][method],
                 label=plot_styles['names'][method],
                 alpha=plot_styles['alpha'][method],
                 linewidth=plot_styles['width'][method])

    if bLegend:
        plt.legend()

    plt.savefig(config['plot_img_path'] +
                "wr" + str(config['writing_rate']) + "_" + 'v{}.png'.format(id),
                bbox_inches='tight')  # , dpi=300)
    if bDisplay:
        plt.show(block=True)

def copy_results(video_ids):
    import shutil
    for id in video_ids:
        common_part = "wr" + str(config['writing_rate']) + "_" + 'v{}.png'.format(id)
        input_file = config['plot_img_path'] + common_part
        output_file = config['plot_path'] + common_part
        print("Src file:",input_file)
        print("Dst file:",output_file)
        shutil.copy(input_file, output_file)


if __name__ == "__main__":
    ids = load_SAVAM_filenames()
    bAll = False
    if bAll:
        selected_ids = ids
    else:
        selected_ids = [22, 30, 43, 36]
    for video_id in selected_ids:
        glimpse_scores, interest_scores, kts_scores = load_scores(video_id)
        plot_scores(video_id, glimpse_scores, interest_scores, kts_scores, bLegend=video_id in selected_ids[:1])

    copy_results(selected_ids)