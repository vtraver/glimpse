video=$1 
method=$2
if [ ! -f "${method}/${video}_${method}.avi" ];
then
	echo "Skipping non existing ${method}/${video}_${method}.avi"
	exit
fi
if [ ! -d ${method}/${video} ];
then
	mkdir ${method}/${video}
fi
ffmpeg -i ${method}/${video}_${method}.avi ${method}/${video}/frame_%04d.pgm

