def get_allFiles_ids_names_SAVAM(path):
    from pathlib import Path
    import re
    leftRight='left'
    files = [f.as_posix() for f in Path(path).rglob('v??'.zfill(2) + "*"+leftRight+"*.avi")]
    names = [re.search(r'\w*_left', file).group()[4:-5] for file in files]
    ids=[re.search(r'v[0-9][0-9]',file).group()[1:] for file in files]
    videos_id2names = {int(id):name for id,name in zip(ids,names)}
    names2id = {name: id for id, name in videos_id2names.items()}
    id2files={int(ids[i]):files[i] for i in range(len(files))}
    return files,videos_id2names, names2id,id2files
