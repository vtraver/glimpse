# GLIMPSE: A Measure of temporal salience from gaze

Code and data corresponding to 

V. Javier Traver, Judith Zorío, Luis A. Leiva: [**GLIMPSE: A Gaze-Based Measure of Temporal Salience.**](https://doi.org/10.3390/s21093099) *Sensors* 21(9): 3099 (2021)

```bib
@article{Traver20sensors,
  author    = {V. Javier Traver and Judith Zor{\'{\i}}o and Luis A. Leiva},
  title     = {{GLIMPSE}: {A} Gaze-Based Measure of Temporal Salience},
  journal   = {Sensors},
  volume    = 21,
  number    = 9,
  year      = 2021,
  url       = {https://doi.org/10.3390/s21093099},
  doi       = {10.3390/s21093099},
}
```
